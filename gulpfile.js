'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var cssmin = require('gulp-cssmin');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;
var fileinclude = require('gulp-file-include');
var productionPath = '../rentalbox-bootstrap-site/public/';

gulp.task('sass', function () {
  return gulp.src(['src/sass/main.scss'])
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('build-dev/css'));
});

gulp.task('browser-sync', function () {
  browserSync.init({
    server: {
      baseDir: "./build-dev/"
    }
  });

  gulp.watch("./build-dev/*.html").on("change", reload);
  gulp.watch("./build-dev/css/*.css").on("change", reload);
  gulp.watch("./build-dev/js/*.js").on("change", reload);
});

gulp.task('cssmin:production', function () {
  gulp.src([
    './build-dev/css/main.css'
    ])
    .pipe(concat('main.css'))
    .pipe(cssmin())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest(productionPath + 'css/'));

  gulp.src('./node_modules/malihu-custom-scrollbar-plugin/mCSB_buttons.png')
    .pipe(gulp.dest(productionPath + '/css'))
});

gulp.task('concat', function () {
  return gulp.src([
    './src/js/jquery.js',
    './src/js/popper.js',
    './node_modules/moment/min/moment-with-locales.min.js',
    './node_modules/sweetalert2/dist/sweetalert2.min.js',
    './src/js/libraries/*.js',
    'src/js/custom.js',
    'src/js/datepickers.js'
  ])
    .pipe(concat('build.js'))
    .pipe(gulp.dest('build-dev/js/'));
});

gulp.task('compress:production', function () {
  return gulp.src('./build-dev/js/build.js')
    .pipe(uglify())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest(productionPath + 'js/'));
});

gulp.task('watch', function () {
  gulp.watch('./src/sass/**/*.scss', ['sass']);
  gulp.watch('./src/sass/**/*.sass', ['sass']);
  gulp.watch('./src/js/custom.js', ['concat']);
  gulp.watch('./src/[^_]*.html', ['html:build']);
});

gulp.task('html:build', function () {
  gulp.src('src/[^_]*.html')
    .pipe(fileinclude({
      prefix: '@@',
      basepath: '@file'
    }))
    .pipe(gulp.dest('./build-dev/'));
});

gulp.task('build:dev', function () {
  gulp.start('html:build', 'concat', 'sass', 'assetsCopy');
});

gulp.task('build:production', function () {
  gulp.start('concat', 'sass');
  setTimeout(function () {
    gulp.start('cssmin:production', 'compress:production');
  }, 3000);
});

gulp.task('build:production-all', function () {
  gulp.start('compress:production', 'cssmin:production', 'image:production', 'assetsCopy:production');
});

gulp.task('assetsCopy', function () {
  gulp.src(['./assets/**/*']).pipe(gulp.dest('./build-dev'));
});

gulp.task('assetsCopy:production', function () {
  gulp.src(['./assets/webfonts/*']).pipe(gulp.dest(productionPath + 'webfonts/'));
  gulp.src(['./assets/video/*']).pipe(gulp.dest(productionPath + 'video/'));
});

gulp.task('default', function () {
  gulp.start('watch', 'browser-sync', 'html:build');
});
