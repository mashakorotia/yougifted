function bindCalendars(dateFromEl, timeFromEl, dateToEl, timeToEl) {

	function getCurrentFromDatetime() {
		var fromDatetime = moment($(dateFromEl).val(), 'DD.MM.YYYY').toDate();
		var fromTime = moment($(timeFromEl).val(), 'HH:mm').toDate();
		fromDatetime.setHours(fromTime.getHours(), fromTime.getMinutes(), 0);
		return fromDatetime;
	}

	function getCurrentToDatetime() {
		var toDatetime = moment($(dateToEl).val(), 'DD.MM.YYYY').toDate();
		var toTime = moment($(timeToEl).val(), 'HH:mm').toDate();
		toDatetime.setHours(toTime.getHours(), toTime.getMinutes(), 0);
		return toDatetime;
	}

	var defaultFromDatetime = getCurrentFromDatetime();
	var defaultToDatetime = getCurrentToDatetime();

	function onRenderCell(date, cellType) {
		if (dateFromDatepicker && dateToDatepicker) {
			var currentFromDate = dateFromDatepicker.selectedDates[0];
			var currentToDate = dateToDatepicker.selectedDates[0];

			if (moment(date).format('DD.MM.YYYY') === moment(currentFromDate).format('DD.MM.YYYY')) {
				return {html: '', classes: '-range-from-', disabled: false};
			}
			else if (moment(date).format('DD.MM.YYYY') === moment(currentToDate).format('DD.MM.YYYY')) {
				return {html: '', classes: '-range-to-', disabled: false};
			}
			else if (moment(date) > moment(currentFromDate) && moment(date) < moment(currentToDate)) {
				return {html: '', classes: '-in-range-', disabled: false};
			}
		}
	}

	function validateFromDate() {
		if (moment().add(1, 'days') > moment(getCurrentFromDatetime())) {
			var less_than_24_hours_message = window.alertMessages && window.alertMessages.less_than_24_hours
                ? window.alertMessages.less_than_24_hours
				: 'Для того, чтобы сделать заказ на аренду автомобиля менее, чем за 24 часа, пожалуйста, обратитесь в Call Centre по телефону 8 800 111-11-11';

			swal('', less_than_24_hours_message);
			return false;
		}
		return true;
	}

	function moveFromDateEarlier() {
		// Двигаем дату начала
		if (moment(getCurrentToDatetime()) < moment(getCurrentFromDatetime()).add(1, 'days')) {
			var newDateFromDate = moment(getCurrentToDatetime()).subtract(1, 'days').toDate();
			$(timeFromEl).val(moment(newDateFromDate).format('HH:mm'));
			dateFromDatepicker.selectDate(newDateFromDate);
			$(timeFromEl).trigger('change');
		}
	}

	function moveToDateLater() {
		// Двигаем дату окончания
		if (moment(getCurrentFromDatetime()).add(1, 'days') > moment(getCurrentToDatetime())) {
			var newDateToDate = moment(getCurrentFromDatetime()).add(1, 'days').toDate();
			$(timeToEl).val(moment(newDateToDate).format('HH:mm'));
			dateToDatepicker.selectDate(newDateToDate);
			$(timeToEl).trigger('change');
		}
	}
	var dateFromDatepicker = $(dateFromEl)
		.datepicker({
			dateFormat: 'dd.mm.yyyy',
			showEvent: 'click',
			autoClose: true,
			toggleSelected: false,
			minDate: moment().toDate(),
			onSelect: function (value, dateObject, datePicker) {
				validateFromDate();
				moveToDateLater();

				// Ограничим дату окончания
				// dateToDatepicker.update('maxDate', moment(getCurrentFromDatetime()).add(3, 'days').toDate());

				$(datePicker.el).trigger('change');
			},
			onRenderCell: onRenderCell
		})
		.data('datepicker');

	var dateToDatepicker = $(dateToEl)
		.datepicker({
			dateFormat: 'dd.mm.yyyy',
			showEvent: 'click',
			autoClose: true,
			toggleSelected: false,
			minDate: moment().add(2, 'days').toDate(),
			onSelect: function (value, dateObject, datePicker) {
				moveFromDateEarlier();

				// Просто чтобы обновить рендеринг
				dateFromDatepicker.update('minDate', dateFromDatepicker.minDate);

				$(datePicker.el).trigger('change');
			},
			onRenderCell: onRenderCell
		})
		.data('datepicker');

	dateFromDatepicker.selectDate(defaultFromDatetime);
	dateToDatepicker.selectDate(defaultToDatetime);

	$(timeFromEl).on('change', function () {
		validateFromDate();
		moveToDateLater();
	});
	$(timeToEl).on('change', function () {
		moveFromDateEarlier();
	});

	$(timeFromEl).val(moment(defaultFromDatetime).format('HH:mm')).trigger('change');
	$(timeToEl).val(moment(defaultToDatetime).format('HH:mm')).trigger('change');
}

jQuery(document).ready(function () {
	if ($('.js-search-form').length > 0) {
		bindCalendars(
			$('.js-search-form [name="from_date"]'),
			$('.js-search-form [name="from_time"]'),
			$('.js-search-form [name="to_date"]'),
			$('.js-search-form [name="to_time"]')
		);
	}
});
